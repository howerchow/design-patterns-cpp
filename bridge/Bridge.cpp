/*
 * C++ Design Patterns: Bridge
 * Author: Jakub Vojvoda [github.com/JakubVojvoda]
 * 2016
 *
 * Source code is licensed under MIT License
 * (for more details see LICENSE)
 *
 */

#include <iostream>

/*
 * Implementor
 * defines the interface for implementation classes
 */
class Implementor
{
      public:
        virtual ~Implementor() = default;

        Implementor( const Implementor& ) = delete;
        Implementor& operator=( const Implementor& ) = delete;

        Implementor( Implementor&& ) = delete;
        Implementor& operator=( Implementor&& ) = delete;

        virtual void action() = 0;
        // ...

      protected:
        Implementor() = default;
};

/*
 * Concrete Implementors
 * implement the Implementor interface and define concrete implementations
 */
class ConcreteImplementorA : public Implementor
{
      public:
        ~ConcreteImplementorA() override = default;

        void action() override
        {
                std::cout << "Concrete Implementor A" << std::endl;
        }
        // ...
};

class ConcreteImplementorB : public Implementor
{
      public:
        ~ConcreteImplementorB() override = default;

        void action() override
        {
                std::cout << "Concrete Implementor B" << std::endl;
        }
        // ...
};

/*
 * Abstraction
 * defines the abstraction's interface
 */
class Abstraction
{
      public:
        virtual ~Abstraction() = default;

        Abstraction( const Abstraction& ) = delete;
        Abstraction& operator=( const Abstraction& ) = delete;

        Abstraction( Abstraction&& ) = delete;
        Abstraction& operator=( Abstraction&& ) = delete;

        virtual void operation() = 0;
        // ...

      protected:
        Abstraction() = default;
};

/*
 * RefinedAbstraction
 * extends the interface defined by Abstraction
 */
class RefinedAbstraction : public Abstraction
{
      public:
        explicit RefinedAbstraction( Implementor* impl ) : implementor { impl } {}
        ~RefinedAbstraction() override = default;

        void operation() override
        {
                implementor->action();
        }
        // ...

      private:
        Implementor* implementor;
};

int main()
{
        Implementor* ia = new ConcreteImplementorA {};
        Abstraction* abstract1 = new RefinedAbstraction { ia };
        abstract1->operation();

        Implementor* ib = new ConcreteImplementorB {};
        Abstraction* abstract2 = new RefinedAbstraction { ib };
        abstract2->operation();

        delete abstract1;
        delete abstract2;

        delete ia;
        delete ib;

        return 0;
}
