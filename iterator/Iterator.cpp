/*
 * C++ Design Patterns: Iterator
 * Author: Jakub Vojvoda [github.com/JakubVojvoda]
 * 2016
 *
 * Source code is licensed under MIT License
 * (for more details see LICENSE)
 *
 */

#include <cstddef>
#include <iostream>

// class Iterator;
// class ConcreteAggregate;

/*
 * Iterator
 * provides the interface that all iterators must implement and
 * a set of methods for traversing over elements
 */
class Iterator
{
      public:
        virtual ~Iterator() = default;

        virtual void first() = 0;
        virtual void next() = 0;
        virtual bool isDone() const = 0;
        virtual int currentItem() const = 0;
        // ...
};

/*
 * Aggregate
 * defines an interface for aggregates and it decouples your
 * client from the implementation of your collection of objects
 */
class Aggregate
{
      public:
        virtual ~Aggregate() = default;
        virtual Iterator* createIterator() = 0;
        // ...
};

/*
 * Concrete Aggregate
 * has a collection of objects and implements the method
 * that returns an Iterator for its collection
 *
 */
class ConcreteAggregate : public Aggregate
{
      public:
        explicit ConcreteAggregate( const std::size_t size )
        {
                list = new int[ size ] { 5, 4, 3, 2, 1 };
                count = size;
        }

        ~ConcreteAggregate() override
        {
                delete[] list;
        }

        Iterator* createIterator() override;

        std::size_t size() const
        {
                return count;
        }

        int at( std::size_t index )
        {
                int value { 0 };
                if ( index >= 0 && index < count )
                {
                        value = list[ index ];
                }
                return value;
        }
        // ...

      private:
        int* list;
        std::size_t count;
        // ...
};

/*
 * Concrete Iterator
 * implements the interface and is responsible for managing
 * the current position of the iterator
 */
class ConcreteIterator : public Iterator
{
      public:
        explicit ConcreteIterator( ConcreteAggregate* l ) : list { l }, index { 0 } {}

        ~ConcreteIterator() override = default;

        void first() override
        {
                index = 0;
        }

        void next() override
        {
                ++index;
        }

        bool isDone() const override
        {
                return index >= list->size();
        }

        int currentItem() const override
        {
                if ( isDone() )
                {
                        return -1;
                }
                return list->at( index );
        }
        // ...

      private:
        ConcreteAggregate* list;
        std::size_t index;
        // ...
};

Iterator* ConcreteAggregate::createIterator()
{
        return new ConcreteIterator { this };
}

int main()
{
        constexpr std::size_t size = 5;
        ConcreteAggregate list = ConcreteAggregate { size };

        Iterator* it = list.createIterator();
        for ( ; !it->isDone(); it->next() )
        {
                std::cout << "Item value: " << it->currentItem() << std::endl;
        }

        delete it;

        return 0;
}
