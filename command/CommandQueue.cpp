/*
 * C++ Design Patterns: Command
 * Author: Jakub Vojvoda [github.com/JakubVojvoda]
 * 2016
 *
 * Source code is licensed under MIT License
 * (for more details see LICENSE)
 *
 */

#include <iostream>
#include <vector>

/*
 * Receiver
 * knows how to perform the operations associated
 * with carrying out a request
 */
class Receiver
{
      public:
        void action()
        {
                std::cout << "Receiver: execute action" << std::endl;
        }
        // ...
};

/*
 * Command
 * declares an interface for all commands
 */
class Command
{
      public:
        virtual ~Command() = default;
        virtual void execute() = 0;
        // ...

      protected:
        Command() = default;
};

/*
 * Concrete Command
 * implements execute by invoking the corresponding
 * operation(s) on Receiver
 */
class ConcreteCommand : public Command
{
      public:
        explicit ConcreteCommand( Receiver* r ) : receiver { r } {}

        ~ConcreteCommand()
        {
                if ( receiver )
                {
                        delete receiver;
                }
        }

        void execute() override
        {
                receiver->action();
        }
        // ...

      private:
        Receiver* receiver;
        // ...
};

class CommandQueue
{
      public:
        CommandQueue() = default;
        ~CommandQueue()
        {
                for ( auto const& it : commands )
                {
                        delete it;
                }
        }

        void add( Command* command )
        {
                commands.push_back( command );
        }

        void execute()
        {
                for ( auto const& it : commands )
                {
                        it->execute();
                }
        }

      private:
        std::vector<Command*> commands;
};

/*
 * Invoker
 * asks the command to carry out the request
 */
class Invoker
{
      public:
        ~Invoker() = default;

        void set( CommandQueue* cq )
        {
                commandQueue = cq;
        }

        void confirm()
        {
                if ( commandQueue )
                {
                        commandQueue->execute();
                }
        }
        // ...

      private:
        CommandQueue* commandQueue;
};

int main()
{
        CommandQueue commandQueue;
        for ( std::size_t i = 0; i < 5; ++i )
        {
                Command* command = new ConcreteCommand { new Receiver {} };
                commandQueue.add( command );
        }

        Invoker invoker;
        invoker.set( &commandQueue );
        invoker.confirm();

        return 0;
}
