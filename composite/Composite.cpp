/*
 * C++ Design Patterns: Composite
 * Author: Jakub Vojvoda [github.com/JakubVojvoda]
 * 2016
 *
 * Source code is licensed under MIT License
 * (for more details see LICENSE)
 *
 */

#include <cstddef>
#include <iostream>
#include <vector>

/*
 * Component
 * defines an interface for all objects in the composition
 * both the composite and the leaf nodes
 */
class Component
{
      public:
        virtual ~Component() = default;

        virtual Component* getChild( int )
        {
                return nullptr;
        }

        virtual void add( Component* )
        { /* ... */
        }
        virtual void remove( int )
        { /* ... */
        }

        virtual void operation() = 0;
};

/*
 * Composite
 * defines behavior of the components having children
 * and store child components
 */
class Composite : public Component
{
      public:
        Composite() = default;
        ~Composite() override
        {
                // for ( std::size_t i = 0; i < children.size(); i++ )
                // {
                //         delete children[ i ];
                // }
                for ( const auto& child : children )
                {
                        delete child;
                }
        }

        Component* getChild( const unsigned int index )
        {
                return children[ index ];
        }

        void add( Component* component ) override
        {
                children.push_back( component );
        }

        void remove( const unsigned int index )
        {
                Component* child = children[ index ];
                children.erase( children.begin() + index );
                delete child;
        }

        void operation() override
        {
                // for ( std::size_t i = 0; i < children.size(); ++i )
                // {
                //         children[ i ]->operation();
                // }
                for ( const auto& child : children )
                {
                        child->operation();
                }
        }

      private:
        std::vector<Component*> children;
};

/*
 * Leaf
 * defines the behavior for the elements in the composition,
 * it has no children
 */
class Leaf : public Component
{
      public:
        explicit Leaf( std::size_t i ) : id { i } {}

        ~Leaf() override = default;

        void operation() override
        {
                std::cout << "Leaf " << id << " operation" << std::endl;
        }

      private:
        std::size_t id;
};

int main()
{
        // Composite composite;

        // for ( std::size_t i = 0; i < 5; i++ )
        // {
        //   composite.add( new Leaf( i ) );
        // }

        // composite.remove( 0 );
        // composite.operation();

        Component* component1 = new Composite {};
        for ( std::size_t i = 0; i < 5; i++ )
        {
                component1->add( new Leaf { i } );
        }

        Component* component2 = new Composite {};
        component2->add( component1 );
        for ( std::size_t i = 5; i < 10; i++ )
        {
                component2->add( new Leaf { i } );
        }

        component2->operation();

        delete component2;

        return 0;
}
