/*
 * C++ Design Patterns: Factory Method
 * Author: Jakub Vojvoda [github.com/JakubVojvoda]
 * 2016
 * Hower Chow
 * 2023 -
 * Source code is licensed under MIT License
 * (for more details see LICENSE)
 *
 */

#include <iostream>
#include <memory>
#include <string>

/*
 * Product
 * products implement the same interface so that the classes can refer
 * to the interface not the concrete product
 */
class Product
{
      public:
        virtual ~Product() = default;
        virtual std::string getName() = 0;
        // ...
};

/*
 * Concrete Product
 * define product to be created
 */
class ConcreteProductA : public Product
{
      public:
        ~ConcreteProductA() override = default;

        std::string getName() override
        {
                return "type A";
        }
        // ...
};

/*
 * Concrete Product
 * define product to be created
 */
class ConcreteProductB : public Product
{
      public:
        ~ConcreteProductB() override = default;

        std::string getName() override
        {
                return "type B";
        }
        // ...
};

/*
 * Factory
 * contains the implementation for all of the methods
 * to manipulate products except for the factory method
 */
class Factory
{
      public:
        virtual ~Factory() = default;
        virtual std::unique_ptr<Product> factoryMethod() = 0;
};

/*
 * Concrete Factory
 * implements factory method that is responsible for creating
 * one or more concrete products ie. it is class that has
 * the knowledge of how to create the products
 */
class ConcreteFactoryA : public Factory
{
      public:
        ~ConcreteFactoryA() override = default;

        std::unique_ptr<Product> factoryMethod() override
        {
                return std::make_unique<ConcreteProductA>();
        }
};

class ConcreteFactoryB : public Factory
{
      public:
        ~ConcreteFactoryB() override = default;

        std::unique_ptr<Product> factoryMethod() override
        {
                return std::make_unique<ConcreteProductB>();
        }
};

int main()
{
        std::unique_ptr<Factory> factory1 = std::make_unique<ConcreteFactoryA>();
        auto p1 = factory1->factoryMethod();
        std::cout << "Product: " << p1->getName() << std::endl;

        std::unique_ptr<Factory> factory2 = std::make_unique<ConcreteFactoryB>();
        auto p2 = factory2->factoryMethod();
        std::cout << "Product: " << p2->getName() << std::endl;

        return 0;
}
