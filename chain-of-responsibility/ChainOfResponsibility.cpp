/*
 * C++ Design Patterns: Chain of Responsibility
 * Author: Jakub Vojvoda [github.com/JakubVojvoda]
 * 2016
 *
 * Source code is licensed under MIT License
 * (for more details see LICENSE)
 *
 */
#include <iostream>

/*
 * Handler
 * defines an interface for handling requests and
 * optionally implements the successor link
 */
class Handler
{
      public:
        virtual ~Handler()
        {
                delete successor;
        }

        virtual void setHandler( Handler* s )
        {
                successor = s;
        }

        virtual void handleRequest()
        {
                if ( successor )
                {
                        successor->handleRequest();
                }
        }
        // ...

      private:
        Handler* successor;
};

/*
 * Concrete Handlers
 * handle requests they are responsible for
 */
class ConcreteHandler1 : public Handler
{
      public:
        ~ConcreteHandler1() override = default;

        bool canHandle()
        {
                // ...
                return false;
        }

        void handleRequest() override
        {
                if ( canHandle() )
                {
                        std::cout << "Handled by Concrete Handler 1" << std::endl;
                }
                else
                {
                        std::cout << "Cannot be handled by Handler 1" << std::endl;
                        Handler::handleRequest(); // Forward Request
                }
                // ...
        }
        // ...
};

class ConcreteHandler2 : public Handler
{
      public:
        ~ConcreteHandler2() override = default;

        bool canHandle()
        {
                // ...
                return true;
        }

        void handleRequest() override
        {
                if ( canHandle() )
                {
                        std::cout << "Handled by Handler 2" << std::endl;
                }
                else
                {
                        std::cout << "Cannot be handled by Handler 2" << std::endl;
                        Handler::handleRequest();
                }
                // ...
        }

        // ...
};

int main()
{
        // ConcreteHandler1 handler1;
        // ConcreteHandler2 handler2;

        // handler1.setHandler( &handler2 );
        // handler1.handleRequest();

        Handler* handler1 = new ConcreteHandler1 {};
        Handler* handler2 = new ConcreteHandler2 {};
        handler1->setHandler( handler2 );

        handler1->handleRequest();

        delete handler1;

        return 0;
}
