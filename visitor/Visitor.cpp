/*
 * C++ Design Patterns: Visitor
 * Author: Jakub Vojvoda [github.com/JakubVojvoda]
 * 2016
 *
 * Source code is licensed under MIT License
 * (for more details see LICENSE)
 *
 */

#include <iostream>
#include <list>

class Element;
class ConcreteElementA;
class ConcreteElementB;

/*
 * Visitor
 * declares a Visit operation for each class of ConcreteElement
 * in the object structure
 */
class Visitor
{
      public:
        virtual ~Visitor() = default;

        virtual void visitElementA( ConcreteElementA* const element ) = 0;
        virtual void visitElementB( ConcreteElementB* const element ) = 0;
        // ...
};

/*
 * Concrete Visitors
 * implement each operation declared by Visitor, which implement
 * a fragment of the algorithm defined for the corresponding class
 * of object in the structure
 */
class ConcreteVisitor1 : public Visitor
{
      public:
        ~ConcreteVisitor1() override = default;

        void visitElementA( ConcreteElementA* const ) override
        {
                std::cout << "Concrete Visitor 1: Element A visited." << std::endl;
        }

        void visitElementB( ConcreteElementB* const ) override
        {
                std::cout << "Concrete Visitor 1: Element B visited." << std::endl;
        }
        // ...
};

class ConcreteVisitor2 : public Visitor
{
      public:
        ~ConcreteVisitor2() override = default;

        void visitElementA( ConcreteElementA* const ) override
        {
                std::cout << "Concrete Visitor 2: Element A visited." << std::endl;
        }

        void visitElementB( ConcreteElementB* const ) override
        {
                std::cout << "Concrete Visitor 2: Element B visited." << std::endl;
        }
        // ...
};

/*
 * Element
 * defines an accept operation that takes a visitor as an argument
 */
class Element
{
      public:
        virtual ~Element() = default;

        virtual void accept( Visitor& visitor ) = 0;
        // ...
};

/*
 * Concrete Elements
 * implement an accept operation that takes a visitor as an argument
 */
class ConcreteElementA : public Element
{
      public:
        ~ConcreteElementA() override = default;

        void accept( Visitor& visitor ) override
        {
                visitor.visitElementA( this );
        }
        // ...
};

class ConcreteElementB : public Element
{
      public:
        ~ConcreteElementB() override = default;

        void accept( Visitor& visitor ) override
        {
                visitor.visitElementB( this );
        }
        // ...
};

class ObjectStructure
{
      public:
        ObjectStructure() = default;
        ~ObjectStructure()
        {
                for ( auto const& it : list )
                {
                        delete it;
                }
        };

        void accept( Visitor& vistor )
        {
                for ( auto const& it : list )
                {
                        it->accept( vistor );
                }
        }

        void addElement( Element* element )
        {
                list.push_back( element );
        }

        void removeElement( Element* element )
        {
                list.remove( element );
        }

      private:
        std::list<Element*> list;
};

int main()
{
        // ConcreteElementA elementA;
        // ConcreteElementB elementB;

        // ConcreteVisitor1 visitor1;
        // ConcreteVisitor2 visitor2;

        // elementA.accept(visitor1);
        // elementA.accept(visitor2);

        // elementB.accept(visitor1);
        // elementB.accept(visitor2);

        // auto vistor1 = std::make_unique<ConcreteVisitor1>();
        // auto vistor2 = std::make_unique<ConcreteVisitor2>();

        ConcreteVisitor1 visitor1;
        ConcreteVisitor2 visitor2;

        ObjectStructure objstr;
        objstr.addElement( new ConcreteElementA {} );
        objstr.addElement( new ConcreteElementB {} );

        objstr.accept( visitor1 );
        objstr.accept( visitor2 );

        return 0;
}
