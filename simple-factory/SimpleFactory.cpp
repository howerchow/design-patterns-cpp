#include <iostream>
#include <string_view>

class Product
{
      public:
        Product() = default;
        virtual ~Product() = default;

        virtual void MethodSame() = 0;
};

class ConcreateProductA : public Product
{
      public:
        ConcreateProductA()
        {
                std::cout << "Create ConcreateProductA" << std::endl;
        }
        ~ConcreateProductA() override = default;

        void MethodSame() override
        {
                std::cout << "ConcreateProductA::MethodSame()" << std::endl;
        }
};

class ConcreateProductB : public Product
{
      public:
        ConcreateProductB()
        {
                std::cout << "Create ConcreateProductB" << std::endl;
        }
        ~ConcreateProductB() override = default;

        void MethodSame() override
        {
                std::cout << "ConcreateProductB::MethodSame()" << std::endl;
        }
};

class Factory
{
      public:
        Factory() = default;
        ~Factory() = default;

        static Product* GetProduct( std::string_view type )
        {
                Product* product { nullptr };
                if ( type == "A" )
                {
                        product = new ConcreateProductA {};
                }
                else if ( type == "B" )
                {
                        product = new ConcreateProductB {};
                }

                return product;
        }
};

int main()
{

        Product* PA = Factory::GetProduct( "A" );
        PA->MethodSame();

        Product* PB = Factory::GetProduct( "B" );
        PB->MethodSame();

        delete PA;
        delete PB;

        return 0;
}
